Providing legal counsel and representation in the Auburn/Opelika area, Huff Smith Law specializes in family and business law as well as minor criminal charges.

Address: 369 S College St, Auburn, AL 36830, USA

Phone: 334-329-5596
